EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:switches
LIBS:wemos
LIBS:oskar-pcb_v01-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SW_Push SW1
U 1 1 5C2E4C88
P 10000 1950
F 0 "SW1" H 10050 2050 50  0000 L CNN
F 1 "SW_Push" H 10000 1890 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 10000 2150 50  0001 C CNN
F 3 "" H 10000 2150 50  0000 C CNN
	1    10000 1950
	1    0    0    -1  
$EndComp
$Comp
L SW_Push SW3
U 1 1 5C2E4D7B
P 10000 2550
F 0 "SW3" H 10050 2650 50  0000 L CNN
F 1 "SW_Push" H 10000 2490 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 10000 2750 50  0001 C CNN
F 3 "" H 10000 2750 50  0000 C CNN
	1    10000 2550
	1    0    0    -1  
$EndComp
$Comp
L SW_Push SW5
U 1 1 5C2E4DE0
P 9000 2250
F 0 "SW5" H 9050 2350 50  0000 L CNN
F 1 "SW_Push" H 9000 2190 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 9000 2450 50  0001 C CNN
F 3 "" H 9000 2450 50  0000 C CNN
	1    9000 2250
	1    0    0    -1  
$EndComp
$Comp
L SW_Push SW2
U 1 1 5C2E4F89
P 10000 2250
F 0 "SW2" H 10050 2350 50  0000 L CNN
F 1 "SW_Push" H 10000 2190 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 10000 2450 50  0001 C CNN
F 3 "" H 10000 2450 50  0000 C CNN
	1    10000 2250
	1    0    0    -1  
$EndComp
$Comp
L SW_Push SW4
U 1 1 5C2E5032
P 9000 1950
F 0 "SW4" H 9050 2050 50  0000 L CNN
F 1 "SW_Push" H 9000 1890 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 9000 2150 50  0001 C CNN
F 3 "" H 9000 2150 50  0000 C CNN
	1    9000 1950
	1    0    0    -1  
$EndComp
$Comp
L SW_Push SW6
U 1 1 5C2E5093
P 9000 2550
F 0 "SW6" H 9050 2650 50  0000 L CNN
F 1 "SW_Push" H 9000 2490 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 9000 2750 50  0001 C CNN
F 3 "" H 9000 2750 50  0000 C CNN
	1    9000 2550
	1    0    0    -1  
$EndComp
$Comp
L SW_Push SW7
U 1 1 5C2E50F8
P 10000 2850
F 0 "SW7" H 10050 2950 50  0000 L CNN
F 1 "SW_Push" H 10000 2790 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 10000 3050 50  0001 C CNN
F 3 "" H 10000 3050 50  0000 C CNN
	1    10000 2850
	1    0    0    -1  
$EndComp
$Comp
L SW_Push SW8
U 1 1 5C2E5157
P 9000 2850
F 0 "SW8" H 9050 2950 50  0000 L CNN
F 1 "SW_Push" H 9000 2790 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 9000 3050 50  0001 C CNN
F 3 "" H 9000 3050 50  0000 C CNN
	1    9000 2850
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5C2E54C2
P 8350 3400
F 0 "R1" V 8430 3400 50  0000 C CNN
F 1 "10k" V 8350 3400 50  0000 C CNN
F 2 "Resistors_THT:Resistor_Horizontal_RM10mm" V 8280 3400 50  0001 C CNN
F 3 "" H 8350 3400 50  0000 C CNN
	1    8350 3400
	0    -1   1    0   
$EndComp
$Comp
L SW_SPDT SW9
U 1 1 5C2E54ED
P 7600 4600
F 0 "SW9" H 7600 4770 50  0000 C CNN
F 1 "SW_SPDT" H 7600 4400 50  0000 C CNN
F 2 "Button_Switches_THT:SW_E-Switch_EG1224_SPDT_Angled" H 7600 4600 50  0001 C CNN
F 3 "" H 7600 4600 50  0000 C CNN
	1    7600 4600
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 5C2E55BD
P 7950 3550
F 0 "#PWR01" H 7950 3300 50  0001 C CNN
F 1 "GND" H 7950 3400 50  0000 C CNN
F 2 "" H 7950 3550 50  0000 C CNN
F 3 "" H 7950 3550 50  0000 C CNN
	1    7950 3550
	-1   0    0    -1  
$EndComp
Text Label 8200 4500 0    60   ~ 0
VBatt
Wire Wire Line
	7850 3400 8200 3400
Connection ~ 7950 3550
Wire Wire Line
	7950 3550 7950 3400
Connection ~ 7950 3400
Wire Wire Line
	7800 3550 7800 4400
$Comp
L CONN_01X02 P2
U 1 1 5C2E6678
P 8000 4450
F 0 "P2" H 8000 4600 50  0000 C CNN
F 1 "CONN_01X02" V 8100 4450 50  0000 C CNN
F 2 "Connectors_JST:JST_PH_S2B-PH-K_02x2.00mm_Angled" H 8000 4450 50  0001 C CNN
F 3 "" H 8000 4450 50  0000 C CNN
	1    8000 4450
	1    0    0    1   
$EndComp
Wire Wire Line
	7800 4500 7800 4600
Text GLabel 6850 2400 0    60   Input ~ 0
1
Text GLabel 9800 1950 0    60   Input ~ 0
1
Text GLabel 6850 2500 0    60   Input ~ 0
4
Text GLabel 8800 1950 0    60   Input ~ 0
4
Text GLabel 6850 2600 0    60   Input ~ 0
2
Text GLabel 9800 2250 0    60   Input ~ 0
2
Text GLabel 6850 2700 0    60   Input ~ 0
5
Text GLabel 8800 2250 0    60   Input ~ 0
5
Text GLabel 6850 2800 0    60   Input ~ 0
3
Text GLabel 9800 2550 0    60   Input ~ 0
3
Text GLabel 6850 2900 0    60   Input ~ 0
6
Text GLabel 8800 2550 0    60   Input ~ 0
6
Text GLabel 6850 3100 0    60   Input ~ 0
7
Text GLabel 9800 2850 0    60   Input ~ 0
7
Text GLabel 6850 3000 0    60   Input ~ 0
8
Text GLabel 8800 2850 0    60   Input ~ 0
8
Text GLabel 8500 3400 2    60   Input ~ 0
GRND
Text GLabel 10200 1950 2    60   Input ~ 0
GRND
Text GLabel 9200 1950 2    60   Input ~ 0
GRND
Text GLabel 10200 2250 2    60   Input ~ 0
GRND
Text GLabel 9200 2250 2    60   Input ~ 0
GRND
Text GLabel 10200 2550 2    60   Input ~ 0
GRND
Text GLabel 9200 2550 2    60   Input ~ 0
GRND
Text GLabel 10200 2850 2    60   Input ~ 0
GRND
Text GLabel 9200 2850 2    60   Input ~ 0
GRND
$Comp
L GND #PWR02
U 1 1 5C6FE187
P 8000 1900
F 0 "#PWR02" H 8000 1650 50  0001 C CNN
F 1 "GND" H 8000 1750 50  0000 C CNN
F 2 "" H 8000 1900 50  0000 C CNN
F 3 "" H 8000 1900 50  0000 C CNN
	1    8000 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 1900 8000 1900
$Comp
L Wemos_LoLinD32 U1
U 1 1 5C8D876C
P 7350 2850
F 0 "U1" H 7350 3900 60  0000 C CNN
F 1 "Wemos_LoLinD32" H 7350 1750 60  0000 C CNN
F 2 "Wemos:LoLin_D32_Board" H 7900 2400 60  0001 C CNN
F 3 "" H 7900 2400 60  0000 C CNN
	1    7350 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 3550 7950 3550
Wire Wire Line
	6850 3400 6850 4500
Wire Wire Line
	6850 4500 7400 4500
$EndSCHEMATC
